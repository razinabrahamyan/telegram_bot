from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():  # put application's code here
    return render_template('index.html')


@app.route('/coins/')
def coins():  # put application's code here
    coins_data = []
    return render_template('coins/index.html', coins=coins_data)


@app.route('/currencies')
def currencies():  # put application's code here
    return render_template('currencies/index.html')


@app.route('/markets')
def markets():  # put application's code here
    return render_template('markets/index.html')

if __name__ == '__main__':
    app.run()
